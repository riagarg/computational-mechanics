from dolfin import *
from mshr import *
import matplotlib.pyplot as plt
import fenics as fe
import numpy as np
import pandas as pd
import mesh
import os
import re

"""
Self-Written Code for Flow Around a Cylinder
"""

# Constant Variables
T = 8                   # final time 
dt = 1/1600             # time step size
num_steps = int(T/dt)   # number of time steps
mu = 0.001              # dynamic viscosity
rho = 1                 # density

# Create Mesh
channel = Rectangle(Point(0, 0), Point(2.2, 0.41))
cylinder = Circle(Point(0.2, 0.2), 0.05)
domain = channel - cylinder
mesh = generate_mesh(domain, 150) # For Mesh Refinement - Change This Number

# Function Space
V = VectorFunctionSpace(mesh, 'CG', 2)
Q = FunctionSpace(mesh, 'CG', 1)

# Functions
def epsilon(u):
    return sym(nabla_grad(u))
def sigma(u, p):
    return 2*mu*epsilon(u) - p*Identity(len(u))

# Boundary Conditions
class inflow(SubDomain):
    def inside(self, x, on_boundary):
	    return near(x[0], 0) and on_boundary
class outflow(SubDomain):
    def inside(self, x, on_boundary):
	    return near(x[0], 2.2) and on_boundary
class walls(SubDomain):
    def inside(self, x, on_boundary):
	    return (near(x[1], 0) or near(x[1], 0.41)) and on_boundary
class cylinder(SubDomain):
    def inside(self, x, on_boundary):
	    return x[0]>=0.15 and x[0]<=0.25 and x[1]>=0.15 and x[1]<=0.25 and on_boundary
	
inflow_profile = ('4.0*1.5*x[1]*(0.41 - x[1]) / pow(0.41, 2)', '0')

bc = MeshFunction("size_t", mesh, mesh.topology().dim() - 1)
bc.set_all(0)
inflow().mark(bc, 1)
outflow().mark(bc, 2)
walls().mark(bc, 3)
cylinder().mark(bc, 4)

bcu_inflow = DirichletBC(V, Expression(inflow_profile, degree=2), inflow()) 
bcp_outflow = DirichletBC(Q, Constant(0), outflow())
bcu_walls = DirichletBC(V, Constant((0, 0)), walls())
bcu_cylinder = DirichletBC(V, Constant((0, 0)), cylinder())

bcu = [bcu_inflow, bcu_walls, bcu_cylinder]
bcp = [bcp_outflow]
ds = fe.ds(subdomain_data=bc)
    
# Trial/Test Functions
u = TrialFunction(V)
v = TestFunction(V)
p = TrialFunction(Q)
q = TestFunction(Q)

# Time-Step Function Solutions
u_n = Function(V)
u_  = Function(V)
p_n = Function(Q)
p_  = Function(Q)

# Variational Form
U  = 0.5*(u_n + u)
n  = -FacetNormal(mesh)
I = Identity(mesh.geometry().dim())
f  = Constant((0, 0))
k  = Constant(dt)
rho = Constant(rho)
mu = Constant(mu)
nu = Constant(0.001)

# Step 1
F1 = rho*dot((u - u_n) / k, v)*dx + rho*dot(dot(u_n, nabla_grad(u_n)), v)*dx + inner(sigma(U, p_n), epsilon(v))*dx + dot(p_n*n, v)*ds - dot(mu*nabla_grad(U)*n, v)*ds - dot(f, v)*dx
a1 = lhs(F1)
L1 = rhs(F1)

# Step 2
a2 = dot(nabla_grad(p), nabla_grad(q))*dx
L2 = dot(nabla_grad(p_n), nabla_grad(q))*dx - (1/k)*div(u_)*q*dx

# Step 3
a3 = dot(u, v)*dx
L3 = dot(u_, v)*dx - k*dot(nabla_grad(p_ - p_n), v)*dx

# Assemble Meshes
A1 = assemble(a1)
A2 = assemble(a2)
A3 = assemble(a3)

# Apply Boundary Conditions to Meshes
[bc.apply(A1) for bc in bcu]
[bc.apply(A2) for bc in bcp]

# Create XDMF files for Visualization Output
xdmffile_u = XDMFFile('navier_stokes_cylinder/velocity.xdmf')
xdmffile_u.parameters["flush_output"] = True
xdmffile_p = XDMFFile('navier_stokes_cylinder/pressure.xdmf')
xdmffile_p.parameters["flush_output"] = True

# Create progress bar
progress = Progress('Time-stepping', num_steps)

# Solve Problem w/ Time-stepping
data = []
t = 0
for j in range(num_steps):

	# Update current time
	t += dt
	
	# Step 1: Tentative velocity step
	b1 = assemble(L1)
	[bc.apply(b1) for bc in bcu]
	solve(A1, u_.vector(), b1, 'bicgstab', 'hypre_amg')
	
	# Step 2: Pressure correction step
	b2 = assemble(L2)
	[bc.apply(b2) for bc in bcp]
	solve(A2, p_.vector(), b2, 'bicgstab', 'hypre_amg')
	
	# Step 3: Velocity correction step
	b3 = assemble(L3)
	solve(A3, u_.vector(), b3, 'cg', 'sor')

	# Save Solution to File (XDMF/HDF5)
	xdmffile_u.write(u_, t)
	xdmffile_p.write(p_, t)

	# Update previous solution
	u_n.assign(u_)
	p_n.assign(p_)

	# Update progress bar
	set_log_level(LogLevel.PROGRESS)
	progress += 1
	set_log_level(LogLevel.ERROR)
	
	a_1 = Point(0.15, 0.2)
	a_2 = Point(0.25, 0.2)
	p_diff = p_(a_1) - p_(a_2)
	
	u_t = inner(as_vector((n[1], -n[0])), u_)
	D = 2 / 0.1 * (mu / rho * inner(grad(u_t), n) * n[1] - p_ * n[0]) * ds(4)
	L = -2 / 0.1 * (mu / rho * inner(grad(u_t), n) * n[0] + p_ * n[1]) * ds(4)

	#  7 (0.005) to 567 (0.355) sec
	if j >= 7 and j <=567:
		data.append([j, p_diff, assemble(D), assemble(L)])
	
# Save Data
df = pd.DataFrame(data, columns=["Time Step", "Pressure Difference", "Drag Coefficient", "Lift Coefficient"])

# Plots
df.plot(kind='line',x='Time Step',y='Pressure Difference')
plt.autoscale()
plt.savefig('diffP.png')
df.plot(kind='line',x='Time Step',y='Drag Coefficient')
plt.autoscale()
plt.savefig('drag.png')
df.plot(kind='line',x='Time Step',y='Lift Coefficient')
plt.autoscale()
plt.savefig('lift.png')
