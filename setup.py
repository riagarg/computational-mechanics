outline_filename = icepack.datasets.fetch_outline('larsen')

with open(outline_filename, 'r') as outline_file:
    outline = geojson.load(outline_file)

features = [feature['geometry'] for feature in outline['features']]
xmin, ymin, xmax, ymax = np.inf, np.inf, -np.inf, -np.inf
δ = 50e3
for feature in outline['features']:
    for line_string in feature['geometry']['coordinates']:
        xs = np.array(line_string)
        x, y = xs[:, 0], xs[:, 1]
        xmin, ymin = min(xmin, x.min() - δ), min(ymin, y.min() - δ)
        xmax, ymax = max(xmax, x.max() + δ), max(ymax, y.max() + δ)

fig, axes = icepack.plot.subplots()

for feature in outline['features']:
    for line_string in feature['geometry']['coordinates']:
        xs = np.array(line_string)
        axes.plot(xs[:, 0], xs[:, 1], linewidth=2)

for feature in outline['features']:
    for line_string in feature['geometry']['coordinates']:
        xs = np.array(line_string)
        axes.plot(xs[:, 0], xs[:, 1], linewidth=2)

axes.set_xlabel('meters');

geometry = icepack.meshing.collection_to_geo(outline)

with open('larsen.geo', 'w') as geo_file:
    geo_file.write(geometry.get_code())
    
cmd1 = "gmsh -2 larsen.geo -format msh2"	
cmd2 = "dolfin-convert larsen.msh larsen.xml"
os.system(cmd1)   
os.system(cmd2)
