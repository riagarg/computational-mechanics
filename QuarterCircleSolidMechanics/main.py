from __future__ import print_function
from fenics import *
from dolfin import *
from mshr import *
from ufl import nabla_div
import matplotlib.pyplot as plt
import fenics as fe
import numpy as np
from numpy import array
import random
import quartGeo
import plots
import errorPlot
import math
import os
import re

"""
main.py // class: main

This class recieves user input and runs all the classes to produce output.

"""

cS = []
r = 0
l = 0

while True:
	try:
		q1 = float(input("Enter Dimension Size for Radius of Circle: "))
		if q1 <= 0:
			raise ValueError
		if q1 == None:
			raise ValueError
		r = q1
		break
	except ValueError:
		print("Not Valid Input... Try Again")
while True:
	try:
		q2 = float(input("Enter Dimension Size for Square: "))
		if q2 <= 0:
			raise ValueError
		if q2 == None:
			raise ValueError
		l = q2
		break
	except ValueError:
		print("Not Valid Input... Try Again")
while True:
	try:
		n = int(input("Enter Number of Refinement Levels Desired: "))
		if n <= 0:
			raise ValueError
		if n == None:
			raise ValueError
		break
	except ValueError:
		print("Not Valid Input... Try Again")
while True:
	try:
		for i in range(0, n):
			ele = float(input("Enter Level {} Refinement: ".format(i+1)))
			if ele <= 0:
				raise ValueError
			if ele == None:
				raise ValueError
			cS.append(ele)
		break
	except ValueError:
		print("Not Valid Input... Try Again")	
# to further organization... ask user for solution name and make that a folder that all documents get stored into
print("Now Calculating Problem ...")
	
quartGeo.meshGeo(cS, r, l)
	
# Define Function G such that G \cdot n = g
class BoundarySource(UserExpression):
	def __init__(self, mesh, **kwargs):
		self.mesh = mesh
		super().__init__(**kwargs)
	def eval_cell(self, values, x, ufc_cell):
		cell = Cell(self.mesh, ufc_cell.index)
		n = cell.normal(ufc_cell.local_facet)
		xC = x[0]
		yC = x[1]
		rl = math.sqrt(xC**2 + yC**2)
		if (xC != 0):
			Theta = math.atan(yC/xC)
		else:
			Theta = 0;
		aN = r
		sigxx = 1 - (pow(aN, 2)/pow(rl, 2)) * (((3/2)*cos(2*Theta)) + cos(4*Theta)) + ((3/2)*(pow(aN, 4)/pow(rl, 4))*cos(4*Theta))
		sigyy = - ((pow(aN, 2)/pow(rl, 2)) * (((1/2)*cos(2*Theta)) - cos(4*Theta))) - ((3/2) * (pow(aN, 4)/pow(rl, 4)) * cos(4*Theta))
		sigxy = - ((pow(aN, 2)/pow(rl, 2)) * (((1/2)*sin(2*Theta)) + sin(4*Theta))) + ((3/2) * (pow(aN, 4)/pow(rl, 4)) * sin(4*Theta))
		sigyx = sigxy
		values[0] = sigxx*n[0] + sigxy*n[1]
		values[1] = sigyx*n[0] + sigyy*n[1]
	def value_shape(self):
		return (2,)

# Define Strain
def epsilon(u):
	return 0.5*(nabla_grad(u) + nabla_grad(u).T)

# Define Stress
def sigma(u):
	return lambda_*nabla_div(u)*Identity(d) + 2*mu*epsilon(u)
	
# Bottom Dirichlet BC
class bottom(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[1], 0) and on_boundary
 
# Left Dirichlet BC   
class left(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0], 0) and on_boundary	
  
# Right NBC   
class right(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0], l) and on_boundary # where l is length of square
        
# Top NBC   
class top(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[1], l) and on_boundary
	
# solving problem
E = 1*10^5
v = 0.3
mu = E / (2*(1 + v))
lambda_ = (E*v) / ((1 + v)*(1 - (2*v)))

# Create Mesh and Define Functions
currPath = os.getcwd()
path = "{}/GMSH/msh/".format(currPath)
fileN = os.listdir(path) # array of msh names
meshN = []
for i in range(len(fileN)):
	pattern = "_(.*?).msh"
	sizeN = re.search(pattern, fileN[i]).group(1)
	meshN.append(sizeN)
	meshN.sort(key = float)
	meshN.sort(reverse = True)
	meshes = [] # all meshes stored into an array
for i in range(len(fileN)):
	meshPath = '{}/GMSH/xml/meshSize_{}.xml'.format(currPath, meshN[i])
	meshes.append(Mesh(meshPath))
	
for i in range(len(meshes)):
	mesh = meshes[i]
	V = VectorFunctionSpace(mesh, "CG", 1)
	G = BoundarySource(mesh, degree=2)
	
	# Define Boundary Conditions
	bc = MeshFunction("size_t", mesh, mesh.topology().dim() - 1)
	bc.set_all(0)
	right().mark(bc, 1)
	top().mark(bc, 2)
	ds = fe.ds(subdomain_data=bc)
	bcs = [DirichletBC(V.sub(1), Constant(0), bottom()), DirichletBC(V.sub(0), Constant(0), left())]

	# Define Variational Problem
	u = TrialFunction(V)
	d = u.geometric_dimension()
	v = TestFunction(V)
	f = Constant((0, 0))
	a = inner(sigma(u), epsilon(v))*dx
	L = dot(f, v)*dx + dot(G, v)*ds(1) + dot(G, v)*ds(2)
	
	# Compute solution
	u = Function(V)
	solve(a == L, u, bcs)
	
	# Save solution to file in VTK format
	vtkfile_u = File('newCircle.pvd')
	vtkfile_u << u
	cmd1 = "mv *.pvd {}/vtk".format(currPath)
	cmd2 = "mv *.vtu {}/vtk".format(currPath)
	os.system(cmd1)
	os.system(cmd2)

	# Plot Solutions
	fN1 = '{}/Plots/Displacement/'.format(currPath)
	fN2 = '{}/Plots/Stress/'.format(currPath)
	name = 'cellSize_{}.png'.format(meshN[i])

	# Displacement
	c1 = plot(u, mode='displacement')
	plt.colorbar(c1)
	plt.savefig(fN1 + name)
	plt.close()
	
	# Stress Contour
	stress = sigma(u)
	sigX = stress[0,0]
	c2 = plot(sigX)
	plt.colorbar(c2)
	plt.savefig(fN2 + name)
	plt.close()
	
	# Plotting Exact vs. Experimental
	plots.calcS(mesh, r, meshN[i], stress, u, G)

# Final Plots
errorPlot.l2Err(meshN)
plots.refinement() # Refinement is not User Input Based - Need to Fix
