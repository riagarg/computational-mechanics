from __future__ import print_function
from fenics import *
from mshr import *
from dolfin import *
from ufl import nabla_div
from textwrap import wrap
import matplotlib.pyplot as plt
import numpy as np
import refinePlot
import math
import os

"""
plots.py // method: calcS

This method obtains data needed to plot stress over theta along with outputting error values to a text file.

"""
allPlt = []

def calcS(mesh, r, meshN, stress, u, G):

	V = VectorFunctionSpace(mesh, "CG", 1)
	bmesh = BoundaryMesh(mesh, "exterior", True)
	list_ =  bmesh.coordinates()

	X = []
	Y = []

	for i in range(len(list_)):
		x = list_[i, 0]
		y = list_[i, 1]
		if y >= 0 and y <= r and x >= 0 and x <= r:
			X.append(x)
			Y.append(y)

	# Order Coordinates for Better Plotting
	X, Y = (list(t) for t in zip(*sorted(zip(X, Y))))	
	X.pop(0)
	X.pop(len(X)-1)
	Y.pop(0)
	Y.pop(len(Y)-1)
	
	# Actual Stress
	sX = []
	degrees = []		
	for i in range(len(X)):
		xC = X[i]
		yC = Y[i]
		rl = math.sqrt(xC**2 + yC**2)
		if (xC != 0):
			Theta = math.atan(yC/xC)
		else:
			Theta = 0;
		aN = r
		sigxx = 1 - (pow(aN, 2)/pow(rl, 2)) * (((3/2)*cos(2*Theta)) + cos(4*Theta)) + ((3/2)*(pow(aN, 4)/pow(rl, 4))*cos(4*Theta))
		sX.append(sigxx)
		degrees.append(math.degrees(Theta))

	currPath = os.getcwd()
	p1 = '{}/Plots/Actual/'.format(currPath)
	p2 = '{}/Plots/Experimental/'.format(currPath)
	p3 = '{}/Plots/Overlap/'.format(currPath)
	name = 'cellSize_{}.png'.format(meshN)

	plt.scatter(sX, degrees)
	plt.savefig(p1 + name)
	plt.close()
	
	# Experimental Solution - Refinement - Checking Accuracy of Stress
	Vsig = TensorFunctionSpace(mesh, "CG", 1)
	sx = project(stress, Vsig)
	sx.set_allow_extrapolation(True)
	exStress = []
	for i in range(len(X)):
		xC = X[i]
		yC = Y[i]
		val = sx(xC, yC)
		exStress.append(val[0])
	plt.scatter(exStress, degrees)
	plt.savefig(p2 + name)
	plt.close()
	
	# Overlapping
	plt.scatter(sX, degrees, s=5, color="red", marker="o")
	plt.plot(sX, degrees, '--r', linewidth=1, label='Actual')
	plt.scatter(exStress, degrees, s=5, color="blue")
	plt.plot(exStress, degrees, '--b', linewidth=1, label='Experimental')
	plt.legend(frameon=True, loc='upper left')
	plt.grid()
	title = 'Comparing Exact and Experimental Values of Normal Stress At Quarter Circle Boundary'
	plt.title('\n'.join(wrap(title,50)), fontsize=12)
	fnorm = '\u03C3\u2093\u2093'
	fdeg = '\u03B8'
	plt.xlabel('Normal Stress, {}'.format(fnorm))
	plt.ylabel('Degrees, {}'.format(fdeg))
	plt.autoscale()
	plt.ylim([0, 90])
	plt.savefig(p3 + name)
	plt.close()
	
	allPlt.append([exStress, degrees])
	allPlt.append([sX, degrees])
	
	# L2 Error
	sX = np.array(sX)
	exStress = np.array(exStress)
	l2 = math.sqrt(np.sum(np.power((sX-exStress),2)))
	filename = "L2_Error.txt"
	f = open(filename, "a")
	f.write("{}\n".format(l2))
	f.close()

def refinement():
	refinePlot.complete()
