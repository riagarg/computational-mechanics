from dolfin import *
import matplotlib.pyplot as plt
import math
import os

"""
quartGeo.py // method: meshGeo

This method serves the function of creating the mesh geometry for a quarter circle.
User input values will be called from outside main class.

"""

def meshGeo(cS, q1, q2):
	for i in range(len(cS)):
		mR = cS[i]
		filename = "meshSize_{}.geo".format(mR)
		fmsh = "meshSize_{}.msh".format(mR)
		fxml = "meshSize_{}.xml".format(mR)
		ql = int(q2)
		r = int(q1)
	
		# code for circular inclusion mesh (gmsh format)
		f = open(filename, "w+")
		f.write("Point(1) = {{0, {}, 0, {}}};\n".format(r, mR))
		f.write("Point(2) = {{0, {}, 0, {}}};\n".format(ql, mR))
		f.write("Point(3) = {{{}, {}, 0, {}}};\n".format(ql, ql, mR))
		f.write("Point(4) = {{{}, 0, 0, {}}};\n".format(ql, mR))
		f.write("Point(5) = {{{}, 0, 0, {}}};\n".format(r, mR))
		f.write("Point(6) = {{0, 0, 0, {}}};\n".format(mR))
		degP = r*math.sqrt(2)/2
		f.write("Point(7) = {{{}, {}, 0, {}}};\n".format(degP, degP, mR))
		f.write("Line(1) = {1, 2};\n")
		f.write("Line(2) = {2, 3};\n")
		f.write("Line(3) = {3, 4};\n")
		f.write("Line(4) = {4, 5};\n")
		f.write("Circle(5) = {1, 6, 7};\n")
		f.write("Circle(6) = {5, 6, 7};\n")
		f.write("Curve Loop(7) = {2, 3, 4, 6, -5, 1};\n")
		f.write("Plane Surface(8) = {7};\n")
		f.write("Physical Line(9) = {1, 5, 6, 4};\n")
		f.write("Physical Line(10) = {2};\n")
		f.write("Physical Line(11) = {3};\n")
		f.write("Physical Surface(12) = {8};\n")
		f.close()
		
		
		cmd1 = "gmsh -2 {} -format msh2".format(filename)
		cmd2 = "dolfin-convert {} {}".format(fmsh, fxml)
		os.system(cmd1)
		os.system(cmd2)
		
		currPath = os.getcwd()
		
		# saving meshed plots		
		fN = os.path.basename("{}/GMSH/{}".format(currPath, fxml))
		mesh = Mesh(fN)
		plot(mesh)
		path = '{}/GMSH/meshPlots/'.format(currPath)
		name = 'cellSize_{}.png'.format(mR)
		plt.savefig(path + name)
		plt.close()
		
		cmd3 = "mv *.xml {}/GMSH/xml".format(currPath)
		cmd4 = "mv *.geo {}/GMSH/geo".format(currPath)
		cmd5 = "mv *.msh {}/GMSH/msh".format(currPath)
		os.system(cmd3)
		os.system(cmd4)
		os.system(cmd5)
