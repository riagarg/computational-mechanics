from dolfin import *
import matplotlib.pyplot as plt
import math
import os

"""
quartGeo.py // method: meshGeo

This method serves the function of creating the mesh geometry for a quarter circle.
User input values will be called from outside main class.

"""

def meshGeo(cSize, q1, q2):
	for i in range(len(cSize)):
		cS = cSize[i]
		filename = "meshSize_{}.geo".format(cS)
		fmsh = "meshSize_{}.msh".format(cS)
		fxml = "meshSize_{}.xml".format(cS)
		a = int(q2)
		r = int(q1)
	
		# code for circular inclusion mesh (gmsh format)
		f = open(filename, "w+")
		f.write("Point(1) = {{0, 0, 0, {}}};\n".format(cS))
		f.write("Point(2) = {{-{}, 0, 0, {}}};\n".format(r, cS))
		f.write("Point(3) = {{{}, 0, 0, {}}};\n".format(r, cS))
		f.write("Point(4) = {{-{}, -{}, 0, {}}};\n".format(a, a, cS))
		f.write("Point(5) = {{{}, -{}, 0, {}}};\n".format(a, a, cS))
		f.write("Point(6) = {{{}, {}, 0, {}}};\n".format(a, a, cS))
		f.write("Point(7) = {{-{}, {}, 0, {}}};\n".format(a, a, cS))
		f.write("Circle(1) = {2, 1, 3};\n")
		f.write("Circle(2) = {3, 1, 2};\n")
		f.write("Line(3) = {7, 4};\n")
		f.write("Line(4) = {4, 5};\n")
		f.write("Line(5) = {5, 6};\n")
		f.write("Line(6) = {6, 7};\n")
		f.write("Line Loop(7) = {3, 4, 5, 6};\n")
		f.write("Line Loop(8) = {2, 1};\n")
		f.write("Plane Surface(9) = {7, 8};\n")
		f.write("Physical Line(10) = {2, 1};\n")
		f.write("Physical Line(11) = {3};\n")
		f.write("Physical Line(12) = {5};\n")
		f.write("Physical Line(13) = {4};\n")
		f.write("Physical Line(14) = {6};\n")
		f.write("Physical Surface(1) = {9};\n")
		f.close()
		
		
		cmd1 = "gmsh -2 {} -format msh2".format(filename)
		cmd2 = "dolfin-convert {} {}".format(fmsh, fxml)
		os.system(cmd1)
		os.system(cmd2)
		
		currPath = os.getcwd()
		
		# saving meshed plots		
		fN = os.path.basename("{}/GMSH/{}".format(currPath, fxml))
		mesh = Mesh(fN)
		plot(mesh)
		path = '{}/GMSH/meshPlots/'.format(currPath)
		name = 'cellSize_{}.png'.format(cS)
		plt.savefig(path + name)
		plt.close()
		
		cmd3 = "mv *.xml {}/GMSH/xml".format(currPath)
		cmd4 = "mv *.geo {}/GMSH/geo".format(currPath)
		cmd5 = "mv *.msh {}/GMSH/msh".format(currPath)
		os.system(cmd3)
		os.system(cmd4)
		os.system(cmd5)
