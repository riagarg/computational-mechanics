import matplotlib.pyplot as plt
import os
import numpy as np
import seaborn as sns
from textwrap import wrap

"""
fullErrorPlot.py

This script plots the error values from a text file over the cell sizes that they were calculated at.

"""

def l2Err(cS):
	fl = open('L2_Error.txt', 'r+')
	Lines = fl.readlines()

	errVal = []
	
	for line in Lines:
		errVal.append(float(line.strip()))
		
	fl.truncate(0)
	fl.close()
	    
	errVal.sort(reverse = True)
	x = [float(i) for i in cS]
	y = [float(i) for i in errVal]
	tL = 'L2 Norm Value: Analyzing Error Value Through Mesh Refinement'
	fig, ax = plt.subplots()
	ax.set_title(tL)
	ax.set(xscale="log", yscale="log")
	plot = sns.regplot(x, y, ci=None)
	currPath = os.getcwd()
	p1 = '{}/Plots/'.format(currPath)
	name = 'l2Error.png'
	plot.set(xlim=(0.05, 1.5))
	plot.set(ylim=(0.1, 10))
	plt.savefig(p1 + name)
	plt.close()
