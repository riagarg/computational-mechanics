import matplotlib.pyplot as plt
import fullPlots
from textwrap import wrap
import os


"""
refinePlot.py

This script plots the refinement levels and the exact values of stress all on one to show the distinctions.

"""

def complete():

	currPath = os.getcwd()
	pLoc = '{}/Plots/'.format(currPath)
	name = 'refinedPlot.png'
	
	p = fullPlots.allPlt
	
	p0 = p[0]
	plt.plot(p0[0], p0[1], '--', linewidth = 1, label ='1.0')
	p2 = p[2]
	plt.plot(p2[0], p2[1], '--', linewidth = 1, label ='0.5')
	p4 = p[4]
	plt.plot(p4[0], p4[1], '--', linewidth = 1, label ='0.25')
	p6 = p[6]
	plt.plot(p6[0], p6[1], '--', linewidth = 1, label ='0.125')
	p8 = p[8]
	plt.plot(p8[0], p8[1], '--', linewidth = 1, label ='0.0625')
	p9 = p[9]
	plt.plot(p9[0], p9[1], linewidth = 1.25, color='black', label ='Actual')
	
	plt.legend(frameon=True, loc='lower right')
	plt.grid()
	title = 'Experimental Mesh Refinment Compared to Most Refined Actual Value of Normal Stress for Full Circle'
	plt.title('\n'.join(wrap(title,60)), fontsize=12)
	fnorm = '\u03C3\u2093\u2093'
	fdeg = '\u03B8'
	plt.xlabel('Normal Stress, {}'.format(fnorm))
	plt.ylabel('Degrees, {}'.format(fdeg))
	plt.autoscale()
	plt.ylim([90, -90])
	plt.savefig(pLoc + name)
	plt.close()
