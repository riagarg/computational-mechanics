import matplotlib.pyplot as plt
import numpy as np

# Opening Data File
f = open('reportPlotData.txt', 'r+')
my_file_data = f.read()
f.close()

# Actual Data for 0.0625
start = '#5 Actual --- 0.0625'
end = '#5 Experimental --- 0.0625'
s = my_file_data
realVal = (s[s.find(start)+len(start):s.rfind(end)])
realVal = realVal.split()
realVal[0] = realVal[0].replace('[', '')
realVal[len(realVal)-1] = realVal[len(realVal)-1].replace(']', '')
realVal = np.array(realVal)
actualData = realVal.astype(float)

# Degrees for 00625
start2 = '#5 Degrees --- 0.0625'
end2 = '\n'
realDeg = (s[s.find(start2)+len(start2):s.rfind(end2)])
realDeg = realDeg.split()
realDeg[0] = realDeg[0].replace('[', '')
realDeg[len(realDeg)-1] = realDeg[len(realDeg)-1].replace(']', '')
realDeg = np.array(realDeg)
degree0625 = realDeg.astype(float)

# Mesh 5 Data
start3 = '#5 Experimental --- 0.0625'
end3 = '#5 Degrees --- 0.0625'
mesh5D = (s[s.find(start)+len(start):s.rfind(end)])
mesh5D = mesh5D.split()
mesh5D[0] = mesh5D[0].replace('[', '')
mesh5D[len(mesh5D)-1] = mesh5D[len(mesh5D)-1].replace(']', '')
mesh5D = np.array(mesh5D)
data5 = mesh5D.astype(float)

# Mesh 5 Degree
degree5 = degree0625

# Mesh 4 Data
start4 = '#4 Experimental --- 0.125'
end4 = '#4 Degrees --- 0.125'
mesh4D = (s[s.find(start)+len(start):s.rfind(end)])
mesh4D = mesh4D.split()
mesh4D[0] = mesh4D[0].replace('[', '')
mesh4D[len(mesh4D)-1] = mesh4D[len(mesh4D)-1].replace(']', '')
mesh4D = np.array(mesh4D)
data4 = mesh4D.astype(float)

# Mesh 4 Degree
start5 = '#4 Degrees --- 0.125'
end5 = '#5 Actual --- 0.0625'
realDeg4 = (s[s.find(start2)+len(start2):s.rfind(end2)])
realDeg4 = realDeg4.split()
realDeg4[0] = realDeg4[0].replace('[', '')
realDeg4[len(realDeg4)-1] = realDeg4[len(realDeg4)-1].replace(']', '')
realDeg4 = np.array(realDeg4)
degree4 = realDeg4.astype(float)

# Overlapping Report Plot
plt.scatter(actualData, degree0625)
plt.plot(data5, degree5)
plt.plot(data4, degree4)
plt.show()
